const cheerio = require('cheerio');
const request = require('request-promise');
const cron = require('node-cron');
const {
    Match
} = require('./models');
require('../database');


class Main {
    static async getMatch() {
        console.log('----------------------------');
        console.log('Loading data, please wait...');
        const $ = await request({
            uri: 'https://www.lcfc.com/matches/results',
            transform: body => cheerio.load(body)
        });
        const matchesStr = $('script[type="application/ld+json"]').html();
        const matchesJSON = await JSON.parse(matchesStr);

        let matches = [];

        const lastMatch = await Match.find().sort({
            "startDate": -1
        }).lean();
        var lastMatchIdFromDB = lastMatch[0].matchId;
        
        for (let i = 0; i < matchesJSON.length; i++) {
            
            let match = {};
            let lastMatchIdFromPage = parseInt(matchesJSON[i].url.substring(27), 10);
            process.stdout.write(`${i}\r`);
            
            if (lastMatchIdFromDB != lastMatchIdFromPage) {
                match.homeTeam = matchesJSON[i].homeTeam.name;
                match.awayTeam = matchesJSON[i].awayTeam.name;
                match.description = matchesJSON[i].description;
                let date = new Date(matchesJSON[i].startDate);
                match.startDate = date;
                match.eventStatus = matchesJSON[i].eventStatus;
                match.url = matchesJSON[i].url;

                const $$ = await request({
                    uri: matchesJSON[i].url,
                    transform: body => cheerio.load(body)
                });

                match.competition = $$(`.mc-header__competition span.u-screen-reader`).text();

                const homeScore = $$(`.scorebox__teams-container 
                    .scorebox__score-container 
                    .js-match-score-container 
                    .score 
                    .home`).text();

                const awayScore = $$(`.scorebox__teams-container 
                    .scorebox__score-container 
                    .js-match-score-container 
                    .score 
                    .away`).text();

                match.homeScore = homeScore;
                match.awayScore = awayScore;

                let matchId = matchesJSON[i].url.substring(27);
                match.matchId = matchId;

                const { homeTeam, awayTeam, description, 
                        startDate, eventStatus, url, competition} = match;

                const newMatch = new Match({ homeTeam, awayTeam, description, startDate,
                                             eventStatus, url, competition, homeScore,
                                             awayScore, matchId });
                await newMatch.save();

                matches.push(match);
            } 
        }
        console.log('Matches added: ');
        console.log(matches);
    } // end getMatch
}


cron.schedule('5 * * * * *', () => {
    Main.getMatch();
});
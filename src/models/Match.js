const { Schema, model } = require('mongoose');

const MatchSchema = new Schema({
    homeTeam: {type: String, required: true},
    awayTeam: {type: String, required: true},
    description: {type: String, required: true},
    startDate: {type: Date, required: true},
    eventStatus: {type: String, required: true},
    url: {type: String, required: true},
    competition: {type: String, required: true},
    homeScore: {type: Number, required: true},
    awayScore: {type: Number, required: true},
    matchId: {type: Number, require: true}
});

module.exports = model('Match', MatchSchema);
const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/leicestercity', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
})
.then(db => console.log('db is connected'))
.catch(err => console.error(new Error(err)));